﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebStatusCodeMonitor {
    [Serializable]
    public class Configuration{
        public int CheckIntervalSeconds { get; set; } = 300;
        public string[] URLsToCheck { get; set; } = {
                "https://google.com",
                "https://httpstat.us/200",
                "https://httpstat.us/400",
                "https://httpstat.us/403",
                "https://httpstat.us/404",
                "https://httpstat.us/405" 
        };
        public string[] DNSServersToCheck { get; set; } = {
            "1.1.1.1",
            "1.0.0.1",
            "8.8.8.8"
        };
        public string[] aRecordQueries { get; set; } = {
            "duckduckgo.com"
        };
        private void saveToFile(string fileURL) {
            //Configuration defaultConfig = new Configuration();
            string jsonString = JsonSerializer.Serialize(this, new JsonSerializerOptions() { WriteIndented = true });
            FileStream newFile = new FileStream(fileURL, FileMode.CreateNew);
            StreamWriter configWriter = new StreamWriter(newFile);
            configWriter.WriteLine(jsonString);
            configWriter.Dispose();
            newFile.Dispose();
        }
        public static Configuration loadFromFile(string fileURL) {
            if (File.Exists(fileURL)) {
                //Load
                try {
                    //Attempt to Load File
                    FileStream configFile = new FileStream(fileURL, FileMode.Open);
                    StreamReader configReader = new StreamReader(configFile);
                    string jsonString = "";
                    string currentLine = "";
                    while ((currentLine = configReader.ReadLine()) != null) {
                        jsonString += currentLine;
                    }
                    configReader.Dispose();
                    configFile.Dispose();
                    Configuration loadedConfig = JsonSerializer.Deserialize<Configuration>(jsonString);
                    return loadedConfig;
                } catch (Exception e) {
                    WebStatusCodeMonitor.eventLog.WriteEntry("The configuration file is inaccessible:\n\n" + e.Message, EventLogEntryType.Error, 201);
                    return null;
                }
            } else {
                Configuration newConfig = new Configuration();
                newConfig.saveToFile(fileURL);
                return newConfig;
            }
        }
    }
}
