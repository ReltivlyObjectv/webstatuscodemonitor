﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WebStatusCodeMonitor {
    internal static class Program {
        static void Main() {
#if DEBUG
            WebStatusCodeMonitor service = new WebStatusCodeMonitor();
            service.OnDebug(new string[] {});
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new WebStatusCodeMonitor()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
