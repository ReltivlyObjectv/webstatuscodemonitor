﻿using DnsClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Policy;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WebStatusCodeMonitor {
    public partial class WebStatusCodeMonitor : ServiceBase {
        public static EventLog eventLog { get; set; } = null;
        private List<string> failedURLs = new List<string>(), failedDNS = new List<string>();
        private Timer intervalTimer;
        private Configuration config;
        private static readonly int SERVICE_STARTED = 100;
        private static readonly int SERVICE_STOPPED = 101;
        private static readonly int URL_GOOD = 200;
        private static readonly int URL_FAIL = 201;
        private static readonly int URL_FAIL_REPEAT = 202;
        private static readonly int DNS_GOOD = 300;
        private static readonly int DNS_FAIL = 301;
        private static readonly int DNS_FAIL_REPEAT = 302;

        public WebStatusCodeMonitor() {
            InitializeComponent();
            config = getConfiguration();

            //Set up Service & Logging
            this.ServiceName = "WebStatusCodeMonitor";
            eventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(this.ServiceName)) {
                System.Diagnostics.EventLog.CreateEventSource(this.ServiceName, "Application");
            }
            eventLog.Source = this.ServiceName;
            eventLog.Log = "Application";
        }
        protected override void OnStart(string[] args) {
            eventLog.WriteEntry("Service Started", EventLogEntryType.Information, SERVICE_STARTED);

            //Run Scan Immediately
            OnTimer(null, null);

            //Set up Timers
            intervalTimer = new Timer();
            intervalTimer.Interval = config.CheckIntervalSeconds * 1000;
            intervalTimer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            intervalTimer.Start();

        }
        protected override void OnStop() {
            eventLog.WriteEntry("Service Stopped", EventLogEntryType.Information, SERVICE_STOPPED);
        }
        private void OnTimer(object sender, ElapsedEventArgs elapsedEventArg) {
            performHTTPChecks();
            performDNSChecks();
        }
#if DEBUG
        public void OnDebug(string[] args) {
            OnTimer(null, null);
        }
#endif
        private void performDNSChecks() {
            foreach (string address in config.DNSServersToCheck) {
                checkDNS(address, config.aRecordQueries);
            }
        }
        private void checkDNS(string dnsAddress, string[] aRecords) {
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(dnsAddress), /*8600*/53);
            LookupClient lookupClient = new LookupClient(endpoint);
            bool success = false;
            string queryLogs = "";
            foreach (string aRecord in aRecords) {
                try {
                    IDnsQueryResponse response = lookupClient.Query(aRecord, QueryType.A);
                    if (response.HasError) {
                        queryLogs += aRecord + " (Error): " + response.ErrorMessage + "\n\n";
                    } else {
                        string records = "";
                        foreach (DnsClient.Protocol.DnsResourceRecord answer in response.Answers) {
                            records += answer.ToString()+"\n";
                        }
                        queryLogs += aRecord + " (Success):\n"+records + "\n\n";
                        success = true;
                    }
                } catch (Exception e) {
                    //Add error to log message
                    queryLogs += aRecord + " (Exception): " + e.ToString() + "\n\n";
                }
            }
            if (success) {
                //Write success event
                string eventMessage = "The DNS server at " + dnsAddress + " successfully responded to at least one DNS query.\n\n" + queryLogs;
#if DEBUG
                Console.WriteLine(eventMessage);
#endif
                eventLog.WriteEntry(eventMessage, EventLogEntryType.Information, DNS_GOOD);
                failedDNS.Remove(dnsAddress);
            } else {
                //Write failure event
                if (!failedDNS.Contains(dnsAddress)) {
                    failedDNS.Add(dnsAddress);
                    string eventMessage = "First Failure: The DNS server at " + dnsAddress + " failed to respond to at least one DNS query.\n\n" + queryLogs;
#if DEBUG
                    Console.WriteLine(eventMessage);
#endif
                    eventLog.WriteEntry(eventMessage, EventLogEntryType.Error, DNS_FAIL);
                } else {
                    string eventMessage = "Repeat Failure: The DNS server at " + dnsAddress + " failed to respond to at least one DNS query.\n\n" + queryLogs;
#if DEBUG
                    Console.WriteLine(eventMessage);
#endif
                    eventLog.WriteEntry(eventMessage, EventLogEntryType.Error, DNS_FAIL_REPEAT);
                }
            }
        }
        private void performHTTPChecks() {
            foreach (string url in config.URLsToCheck) {
                checkURL(url);
            }
        }
        private void checkURL(string url) {
            HttpStatusCode statusCode = GetHeaders(url);
            int statusCodeInt = (int)statusCode;
#if DEBUG
            Console.WriteLine(url+" returned status code "+statusCodeInt);
#endif
            if (statusCodeInt == 200) {
                //Write event if code is 200
                eventLog.WriteEntry("The URL "+url+" returned a status code of 200.", EventLogEntryType.Information, URL_GOOD);
                failedURLs.Remove(url);
            } else {
                //Write event if code is not 200
                if (!failedURLs.Contains(url)) {
                    failedURLs.Add(url);
                    eventLog.WriteEntry("First Failure: The URL " + url + " returned a status code of " + statusCodeInt + ". \n\n" + statusCode.ToString(), EventLogEntryType.Error, URL_FAIL);
                } else {
                    eventLog.WriteEntry("Repeat Failure: The URL " + url + " returned a status code of " + statusCodeInt + ". \n\n" + statusCode.ToString(), EventLogEntryType.Error, URL_FAIL_REPEAT);
                }
            }
        }
        public HttpStatusCode GetHeaders(string url) {
            HttpStatusCode result = default(HttpStatusCode);

            var request = HttpWebRequest.Create(url);
            request.Method = "HEAD";
            try {
                using (var response = request.GetResponse() as HttpWebResponse) {
                    if (response != null) {
                        result = response.StatusCode;
                        response.Close();
                    }
                }
            } catch (System.Net.WebException ex) {
                result = ((HttpWebResponse) ex.Response).StatusCode;
            }

            return result;
        }
        private Configuration getConfiguration() {
            string configPath = Path.Combine(getExecutableDirectory(), "config.json");
            Console.WriteLine(configPath);
            return Configuration.loadFromFile(configPath);
        }
        private static string getExecutableDirectory() {
            string executablePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return executablePath;
        }
    }
}
